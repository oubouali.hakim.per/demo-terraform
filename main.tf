# variable "Keys" {
#   type = list(string)
#   default = ["beta","delta","zeta" ,"aplha","gamma"]
# }  

variable "Keys" {
  type = map(string)
  default = {
    "alphakey" = "beta"
    "betakey"  = "alpha"
  }
}


variable "Keys2" {
  type = map(object({
      name = string
    }))
  default = {
    "alphakey" = {
      name ="beta"
      },
    "betakey"  = {
      name = "alpha"
    }
  }
}

module "name" {
  source = "terraform-aws-modules/key-pair/aws"
  # "git::https://example.com/vpc.git?ref=v1.2.0"
  # source = "git::https://github.com/terraform-aws-modules/terraform-aws-key-pair.git"
  # count = length(var.Keys)
  # key_name           = var.Keys[count.index]
  # create_private_key = true

  #   for_each = toset(var.Keys)
  #   key_name           = each.key
  #   create_private_key = true

  for_each           = var.Keys2
  key_name           = each.value.name
  create_private_key = true


}


# resource "local_sensitive_file" "foo" {
#   content  = "${module.name.private_key_pem}"
#   filename = "${module.name.key_pair_name}.pem"
#   file_permission = "200"
# }